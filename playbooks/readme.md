
### DNS Role

Edit various DNS settings in resolv.conf

Variable | Choices | Type
--- | --- | ---
dns.domain|Single FQDN used for this switch's domain name.|String
dns.search_domain|DNS search domain.|List of Strings
dns.servers.ipv4|DNS server IP addresses.|List of Strings
dns.servers.vrf|VRF used to communicate to DNS server.|List of Strings

##### Example

```bash
---
domain: cumulusnetworks.com
search_domain:
- cumulusnetworks.com
servers:
    ipv4:
    - 1.1.1.1
    - 8.8.8.8
    vrf: mgmt
...

```
###### evpn_centralized
{'results': [{'cmd': ['ls', '/home/cumulus/cumulus_ansible_modules/inventories/evpn_centralized'], 'stdout': 'README.md\nconfig\ngroup_vars\nhost_vars\nhosts', 'stderr': '', 'rc': 0, 'start': '2021-02-16 09:58:18.804713', 'end': '2021-02-16 09:58:18.810627', 'delta': '0:00:00.005914', 'changed': True, 'invocation': {'module_args': {'_raw_params': 'ls /home/cumulus/cumulus_ansible_modules/inventories/evpn_centralized', 'warn': True, '_uses_shell': False, 'stdin_add_newline': True, 'strip_empty_ends': True, 'argv': None, 'chdir': None, 'executable': None, 'creates': None, 'removes': None, 'stdin': None}}, 'stdout_lines': ['README.md', 'config', 'group_vars', 'host_vars', 'hosts'], 'stderr_lines': [], 'failed': False, 'item': 'evpn_centralized', 'ansible_loop_var': 'item'}, {'cmd': ['ls', '/home/cumulus/cumulus_ansible_modules/inventories/evpn_l2only'], 'stdout': 'README.md\nconfig\ngroup_vars\nhost_vars\nhosts', 'stderr': '', 'rc': 0, 'start': '2021-02-16 09:58:19.130211', 'end': '2021-02-16 09:58:19.135650', 'delta': '0:00:00.005439', 'changed': True, 'invocation': {'module_args': {'_raw_params': 'ls /home/cumulus/cumulus_ansible_modules/inventories/evpn_l2only', 'warn': True, '_uses_shell': False, 'stdin_add_newline': True, 'strip_empty_ends': True, 'argv': None, 'chdir': None, 'executable': None, 'creates': None, 'removes': None, 'stdin': None}}, 'stdout_lines': ['README.md', 'config', 'group_vars', 'host_vars', 'hosts'], 'stderr_lines': [], 'failed': False, 'item': 'evpn_l2only', 'ansible_loop_var': 'item'}, {'cmd': ['ls', '/home/cumulus/cumulus_ansible_modules/inventories/evpn_mh'], 'stdout': 'config\ngroup_vars\nhost_vars\nhosts', 'stderr': '', 'rc': 0, 'start': '2021-02-16 09:58:19.456047', 'end': '2021-02-16 09:58:19.462062', 'delta': '0:00:00.006015', 'changed': True, 'invocation': {'module_args': {'_raw_params': 'ls /home/cumulus/cumulus_ansible_modules/inventories/evpn_mh', 'warn': True, '_uses_shell': False, 'stdin_add_newline': True, 'strip_empty_ends': True, 'argv': None, 'chdir': None, 'executable': None, 'creates': None, 'removes': None, 'stdin': None}}, 'stdout_lines': ['config', 'group_vars', 'host_vars', 'hosts'], 'stderr_lines': [], 'failed': False, 'item': 'evpn_mh', 'ansible_loop_var': 'item'}, {'cmd': ['ls', '/home/cumulus/cumulus_ansible_modules/inventories/evpn_symmetric'], 'stdout': 'README.md\nconfig\ngroup_vars\nhost_vars\nhosts', 'stderr': '', 'rc': 0, 'start': '2021-02-16 09:58:19.804893', 'end': '2021-02-16 09:58:19.810865', 'delta': '0:00:00.005972', 'changed': True, 'invocation': {'module_args': {'_raw_params': 'ls /home/cumulus/cumulus_ansible_modules/inventories/evpn_symmetric', 'warn': True, '_uses_shell': False, 'stdin_add_newline': True, 'strip_empty_ends': True, 'argv': None, 'chdir': None, 'executable': None, 'creates': None, 'removes': None, 'stdin': None}}, 'stdout_lines': ['README.md', 'config', 'group_vars', 'host_vars', 'hosts'], 'stderr_lines': [], 'failed': False, 'item': 'evpn_symmetric', 'ansible_loop_var': 'item'}], 'changed': True, 'msg': 'All items completed'}
<details>
  <summary markdown="span">leaf01 dns variables</summary>
---
ansible_check_mode: false
ansible_diff_mode: false
ansible_facts:
    discovered_interpreter_python: /usr/bin/python
ansible_forks: 50
ansible_inventory_sources:
- /home/cumulus/cumulus_ansible_modules/inventories/evpn_centralized/hosts
ansible_playbook_python: /usr/bin/python3
ansible_run_tags:
- backup
- interfaces
- servers
- frr
ansible_skip_tags: []
ansible_ssh_pass: CumulusLinux!
ansible_user: cumulus
ansible_verbosity: 0
ansible_version:
    full: 2.9.13
    major: 2
    minor: 9
    revision: 13
    string: 2.9.13
backup:
    files:
    - /etc/network/interfaces
    - /etc/frr/frr.conf
    - /etc/frr/daemons
    path: ../inventories/evpn_centralized/config/leaf01
bgp:
    address_family:
    -   name: ipv4_unicast
        redistribute:
        -   type: connected
    -   advertise_all_vni: true
        name: l2vpn_evpn
        neighbors:
        -   activate: true
            interface: underlay
    asn: '65101'
    neighbors:
    -   interface: peerlink.4094
        peergroup: underlay
        unnumbered: true
    -   interface: swp51
        peergroup: underlay
        unnumbered: true
    -   interface: swp52
        peergroup: underlay
        unnumbered: true
    -   interface: swp53
        peergroup: underlay
        unnumbered: true
    -   interface: swp54
        peergroup: underlay
        unnumbered: true
    peergroups:
    -   name: underlay
        remote_as: external
    router_id: 10.10.10.1
bgp_asn_prefix: 651
bonds:
-   bridge:
        access: 10
    clag_id: 1
    name: bond1
    options:
        extras:
        - bond-lacp-bypass-allow yes
        - mstpctl-bpduguard yes
        - mstpctl-portadminedge yes
        mtu: 9000
    ports:
    - swp1
-   bridge:
        access: 20
    clag_id: 2
    name: bond2
    options:
        extras:
        - bond-lacp-bypass-allow yes
        - mstpctl-bpduguard yes
        - mstpctl-portadminedge yes
        mtu: 9000
    ports:
    - swp2
bridge:
    vids:
    - 10
    - 20
discovered_interpreter_python: /usr/bin/python
dns:
    domain: cumulusnetworks.com
    search_domain:
    - cumulusnetworks.com
    servers:
        ipv4:
        - 1.1.1.1
        - 8.8.8.8
        vrf: mgmt
eth0:
    ips:
    - 192.168.200.11/24
eth0_id: 11
eth0_ip: 192.168.200.11/24
eth0_subnet: 192.168.200.0/24
fabric_name: evpn_centralized
group_names:
- leaf
- pod1
groups:
    all:
    - netq-ts
    - fw1
    - leaf01
    - leaf02
    - leaf03
    - leaf04
    - spine01
    - spine02
    - spine03
    - spine04
    - border01
    - border02
    - server01
    - server02
    - server04
    - server05
    border:
    - border01
    - border02
    fw:
    - fw1
    leaf:
    - leaf01
    - leaf02
    - leaf03
    - leaf04
    netq:
    - netq-ts
    pod1:
    - fw1
    - leaf01
    - leaf02
    - leaf03
    - leaf04
    - spine01
    - spine02
    - spine03
    - spine04
    - border01
    - border02
    - server01
    - server02
    - server04
    - server05
    server:
    - server01
    - server02
    - server04
    - server05
    spine:
    - spine01
    - spine02
    - spine03
    - spine04
    ungrouped: []
id: 1
interfaces:
-   name: swp51
    options:
        extras:
        - alias to spine
-   name: swp52
    options:
        extras:
        - alias to spine
-   name: swp53
    options:
        extras:
        - alias to spine
-   name: swp54
    options:
        extras:
        - alias to spine
inventory_dir: /home/cumulus/cumulus_ansible_modules/inventories/evpn_centralized
inventory_file: /home/cumulus/cumulus_ansible_modules/inventories/evpn_centralized/hosts
inventory_hostname: leaf01
inventory_hostname_short: leaf01
leaf_spine_interface:
    extras:
    - alias to spine
loopback:
    clag_vxlan_anycast_ip: 10.0.1.12
    ips:
    - 10.10.10.1/32
    vxlan_local_tunnel_ip: 10.10.10.1
mac_prefix: 00:00:00:00:00
mlag:
    backup: 10.10.10.2
    init_delay: 10
    peerlinks:
    - swp49
    - swp50
    priority: primary
    sysmac: 44:38:39:BE:EF:AA
mlagBondProfileA:
    bridge_access: 10
    extras:
    - bond-lacp-bypass-allow yes
    - mstpctl-bpduguard yes
    - mstpctl-portadminedge yes
mlagBondProfileB:
    bridge_access: 20
    extras:
    - bond-lacp-bypass-allow yes
    - mstpctl-bpduguard yes
    - mstpctl-portadminedge yes
mlagBondProfileFW:
    bridge_vids:
    - 101
    - 102
mlag_neighbor: leaf02
mlag_priority: primary
mlag_sysmac_prefix: 44:38:39:BE:EF
netq:
    agent_server: 192.168.200.250
    cli_access_key: long-key-0987654321
    cli_port: 443
    cli_premises: CITC
    cli_secret_key: long-key-1234567890
    cli_server: api.air.netq.cumulusnetworks.com
    version: latest
ntp:
    server_ips:
    - 0.cumulusnetworks.pool.ntp.org
    - 1.cumulusnetworks.pool.ntp.org
    - 2.cumulusnetworks.pool.ntp.org
    - 3.cumulusnetworks.pool.ntp.org
    timezone: America/Los_Angeles
playbook_dir: /home/cumulus/cumulus_ansible_modules/playbooks
snmp:
    addresses:
    - 192.168.200.11/24@mgmt
    - udp6:[::1]:161
    rocommunity: public
spine_leaf_interface:
    extras:
    - alias to leaf
ssh:
    banner: |-
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        Authorized Users Only!
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    motd: |-
        #########################################################
        Successfully logged in to: leaf01
        #########################################################
syslog:
    servers:
    - 192.168.200.1
sysmac: AA
tacacs:
    groups:
    -   name: admins
        priv_level: 15
    -   name: basics
        priv_level: 1
    secret: tacacskey
    server_ips:
    - 192.168.200.1
    users:
    -   group: basics
        name: basicuser
        password: password
    -   group: admins
        name: adminuser
        password: password
    vrf: mgmt
vlan10:
    id: 10
    name: vlan10
vlan101:
    address:
    - 10.1.101.2/24
    address_virtual:
    - 00:00:00:00:00:01 10.1.101.1/24
    id: 101
    name: vlan101
vlan101_subnet: 10.1.101.0/24
vlan102:
    address:
    - 10.1.102.2/24
    address_virtual:
    - 00:00:00:00:00:02 10.1.102.1/24
    id: 102
    name: vlan102
vlan102_subnet: 10.1.102.0/24
vlan10_border:
    address:
    - 10.1.10.2/24
    address_virtual:
    - 00:00:00:00:00:10 10.1.10.1/24
    id: 10
    name: vlan10
vlan10_subnet: 10.1.10.0/24
vlan20:
    id: 20
    name: vlan20
vlan20_border:
    address:
    - 10.1.20.2/24
    address_virtual:
    - 00:00:00:00:00:20 10.1.20.1/24
    id: 20
    name: vlan20
vlan20_subnet: 10.1.20.0/24
vlan30_subnet: 10.1.30.0/24
vlans:
-   id: 10
    name: vlan10
-   id: 20
    name: vlan20
vni10:
    name: vni10
    vlan_id: 10
    vxlan_id: 10
vni20:
    name: vni20
    vlan_id: 20
    vxlan_id: 20
vnis:
-   name: vni10
    vlan_id: 10
    vxlan_id: 10
-   name: vni20
    vlan_id: 20
    vxlan_id: 20
vrf:
-   extras:
    - address 127.0.0.1/8
    - address ::1/128
    name: mgmt
    routes:
    - 0.0.0.0/0 192.168.200.1
vrf_default_FW:
    name: default
    routes:
    - 10.1.10.0/24 10.1.101.1
    - 10.1.20.0/24 10.1.102.1
vrf_mgmt:
    extras:
    - address 127.0.0.1/8
    - address ::1/128
    name: mgmt
    routes:
    - 0.0.0.0/0 192.168.200.1
vxlan_anycast_id: 12
vxlan_anycast_loopback: 10.0.1.12/32
vxlan_local_loopback: 10.10.10.1/32
...
</details>
###### evpn_l2only
{'results': [{'cmd': ['ls', '/home/cumulus/cumulus_ansible_modules/inventories/evpn_centralized'], 'stdout': 'README.md\nconfig\ngroup_vars\nhost_vars\nhosts', 'stderr': '', 'rc': 0, 'start': '2021-02-16 09:58:18.804713', 'end': '2021-02-16 09:58:18.810627', 'delta': '0:00:00.005914', 'changed': True, 'invocation': {'module_args': {'_raw_params': 'ls /home/cumulus/cumulus_ansible_modules/inventories/evpn_centralized', 'warn': True, '_uses_shell': False, 'stdin_add_newline': True, 'strip_empty_ends': True, 'argv': None, 'chdir': None, 'executable': None, 'creates': None, 'removes': None, 'stdin': None}}, 'stdout_lines': ['README.md', 'config', 'group_vars', 'host_vars', 'hosts'], 'stderr_lines': [], 'failed': False, 'item': 'evpn_centralized', 'ansible_loop_var': 'item'}, {'cmd': ['ls', '/home/cumulus/cumulus_ansible_modules/inventories/evpn_l2only'], 'stdout': 'README.md\nconfig\ngroup_vars\nhost_vars\nhosts', 'stderr': '', 'rc': 0, 'start': '2021-02-16 09:58:19.130211', 'end': '2021-02-16 09:58:19.135650', 'delta': '0:00:00.005439', 'changed': True, 'invocation': {'module_args': {'_raw_params': 'ls /home/cumulus/cumulus_ansible_modules/inventories/evpn_l2only', 'warn': True, '_uses_shell': False, 'stdin_add_newline': True, 'strip_empty_ends': True, 'argv': None, 'chdir': None, 'executable': None, 'creates': None, 'removes': None, 'stdin': None}}, 'stdout_lines': ['README.md', 'config', 'group_vars', 'host_vars', 'hosts'], 'stderr_lines': [], 'failed': False, 'item': 'evpn_l2only', 'ansible_loop_var': 'item'}, {'cmd': ['ls', '/home/cumulus/cumulus_ansible_modules/inventories/evpn_mh'], 'stdout': 'config\ngroup_vars\nhost_vars\nhosts', 'stderr': '', 'rc': 0, 'start': '2021-02-16 09:58:19.456047', 'end': '2021-02-16 09:58:19.462062', 'delta': '0:00:00.006015', 'changed': True, 'invocation': {'module_args': {'_raw_params': 'ls /home/cumulus/cumulus_ansible_modules/inventories/evpn_mh', 'warn': True, '_uses_shell': False, 'stdin_add_newline': True, 'strip_empty_ends': True, 'argv': None, 'chdir': None, 'executable': None, 'creates': None, 'removes': None, 'stdin': None}}, 'stdout_lines': ['config', 'group_vars', 'host_vars', 'hosts'], 'stderr_lines': [], 'failed': False, 'item': 'evpn_mh', 'ansible_loop_var': 'item'}, {'cmd': ['ls', '/home/cumulus/cumulus_ansible_modules/inventories/evpn_symmetric'], 'stdout': 'README.md\nconfig\ngroup_vars\nhost_vars\nhosts', 'stderr': '', 'rc': 0, 'start': '2021-02-16 09:58:19.804893', 'end': '2021-02-16 09:58:19.810865', 'delta': '0:00:00.005972', 'changed': True, 'invocation': {'module_args': {'_raw_params': 'ls /home/cumulus/cumulus_ansible_modules/inventories/evpn_symmetric', 'warn': True, '_uses_shell': False, 'stdin_add_newline': True, 'strip_empty_ends': True, 'argv': None, 'chdir': None, 'executable': None, 'creates': None, 'removes': None, 'stdin': None}}, 'stdout_lines': ['README.md', 'config', 'group_vars', 'host_vars', 'hosts'], 'stderr_lines': [], 'failed': False, 'item': 'evpn_symmetric', 'ansible_loop_var': 'item'}], 'changed': True, 'msg': 'All items completed'}
<details>
  <summary markdown="span">leaf01 dns variables</summary>
---
ansible_check_mode: false
ansible_diff_mode: false
ansible_facts:
    discovered_interpreter_python: /usr/bin/python
ansible_forks: 50
ansible_inventory_sources:
- /home/cumulus/cumulus_ansible_modules/inventories/evpn_l2only/hosts
ansible_playbook_python: /usr/bin/python3
ansible_run_tags:
- interfaces
- frr
- backup
- servers
ansible_skip_tags: []
ansible_ssh_pass: CumulusLinux!
ansible_user: cumulus
ansible_verbosity: 0
ansible_version:
    full: 2.9.13
    major: 2
    minor: 9
    revision: 13
    string: 2.9.13
backup:
    files:
    - /etc/network/interfaces
    - /etc/frr/frr.conf
    - /etc/frr/daemons
    path: ../inventories/evpn_l2only/config/leaf01
bgp:
    address_family:
    -   name: ipv4_unicast
        redistribute:
        -   type: connected
    -   advertise_all_vni: true
        name: l2vpn_evpn
        neighbors:
        -   activate: true
            interface: underlay
    asn: '65101'
    neighbors:
    -   interface: peerlink.4094
        peergroup: underlay
        unnumbered: true
    -   interface: swp51
        peergroup: underlay
        unnumbered: true
    -   interface: swp52
        peergroup: underlay
        unnumbered: true
    -   interface: swp53
        peergroup: underlay
        unnumbered: true
    -   interface: swp54
        peergroup: underlay
        unnumbered: true
    peergroups:
    -   name: underlay
        remote_as: external
    router_id: 10.10.10.1
bgp_asn_prefix: 651
bonds:
-   bridge:
        access: 10
    clag_id: 1
    name: bond1
    options:
        extras:
        - bond-lacp-bypass-allow yes
        - mstpctl-bpduguard yes
        - mstpctl-portadminedge yes
        mtu: 9000
    ports:
    - swp1
-   bridge:
        access: 20
    clag_id: 2
    name: bond2
    options:
        extras:
        - bond-lacp-bypass-allow yes
        - mstpctl-bpduguard yes
        - mstpctl-portadminedge yes
        mtu: 9000
    ports:
    - swp2
bridge:
    vids:
    - 10
    - 20
discovered_interpreter_python: /usr/bin/python
dns:
    domain: cumulusnetworks.com
    search_domain:
    - cumulusnetworks.com
    servers:
        ipv4:
        - 1.1.1.1
        - 8.8.8.8
        vrf: mgmt
eth0:
    ips:
    - 192.168.200.11/24
eth0_id: 11
eth0_ip: 192.168.200.11/24
eth0_subnet: 192.168.200.0/24
fabric_name: evpn_l2only
group_names:
- leaf
- pod1
groups:
    all:
    - netq-ts
    - fw1
    - leaf01
    - leaf02
    - leaf03
    - leaf04
    - spine01
    - spine02
    - spine03
    - spine04
    - border01
    - border02
    - server01
    - server02
    - server04
    - server05
    border:
    - border01
    - border02
    fw:
    - fw1
    leaf:
    - leaf01
    - leaf02
    - leaf03
    - leaf04
    netq:
    - netq-ts
    pod1:
    - fw1
    - leaf01
    - leaf02
    - leaf03
    - leaf04
    - spine01
    - spine02
    - spine03
    - spine04
    - border01
    - border02
    - server01
    - server02
    - server04
    - server05
    server:
    - server01
    - server02
    - server04
    - server05
    spine:
    - spine01
    - spine02
    - spine03
    - spine04
    ungrouped: []
id: 1
interfaces:
-   name: swp51
    options:
        extras:
        - alias to spine
-   name: swp52
    options:
        extras:
        - alias to spine
-   name: swp53
    options:
        extras:
        - alias to spine
-   name: swp54
    options:
        extras:
        - alias to spine
inventory_dir: /home/cumulus/cumulus_ansible_modules/inventories/evpn_l2only
inventory_file: /home/cumulus/cumulus_ansible_modules/inventories/evpn_l2only/hosts
inventory_hostname: leaf01
inventory_hostname_short: leaf01
leaf_spine_interface:
    extras:
    - alias to spine
loopback:
    clag_vxlan_anycast_ip: 10.0.1.12
    ips:
    - 10.10.10.1/32
    vxlan_local_tunnel_ip: 10.10.10.1
mlag:
    backup: 10.10.10.2
    init_delay: 10
    peerlinks:
    - swp49
    - swp50
    priority: primary
    sysmac: 44:38:39:BE:EF:AA
mlagBondProfileA:
    bridge_access: 10
    extras:
    - bond-lacp-bypass-allow yes
    - mstpctl-bpduguard yes
    - mstpctl-portadminedge yes
mlagBondProfileB:
    bridge_access: 20
    extras:
    - bond-lacp-bypass-allow yes
    - mstpctl-bpduguard yes
    - mstpctl-portadminedge yes
mlagBondProfileFW:
    bridge_vids:
    - 10
    - 20
mlag_neighbor: leaf02
mlag_priority: primary
mlag_sysmac_prefix: 44:38:39:BE:EF
netq:
    agent_server: 192.168.200.250
    cli_access_key: long-key-0987654321
    cli_port: 443
    cli_premises: CITC
    cli_secret_key: long-key-1234567890
    cli_server: api.air.netq.cumulusnetworks.com
    version: latest
ntp:
    server_ips:
    - 0.cumulusnetworks.pool.ntp.org
    - 1.cumulusnetworks.pool.ntp.org
    - 2.cumulusnetworks.pool.ntp.org
    - 3.cumulusnetworks.pool.ntp.org
    timezone: America/Los_Angeles
playbook_dir: /home/cumulus/cumulus_ansible_modules/playbooks
snmp:
    addresses:
    - 192.168.200.11/24@mgmt
    - udp6:[::1]:161
    rocommunity: public
spine_leaf_interface:
    extras:
    - alias to leaf
ssh:
    banner: |-
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        Authorized Users Only!
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    motd: |-
        #########################################################
        Successfully logged in to: leaf01
        #########################################################
syslog:
    servers:
    - 192.168.200.1
sysmac: AA
tacacs:
    groups:
    -   name: admins
        priv_level: 15
    -   name: basics
        priv_level: 1
    secret: tacacskey
    server_ips:
    - 192.168.200.1
    users:
    -   group: basics
        name: basicuser
        password: password
    -   group: admins
        name: adminuser
        password: password
    vrf: mgmt
vlan10:
    extras:
    - ip-forward off
    - ip6-forward off
    id: 10
    name: vlan10
vlan20:
    extras:
    - ip-forward off
    - ip6-forward off
    id: 20
    name: vlan20
vlans:
-   extras:
    - ip-forward off
    - ip6-forward off
    id: 10
    name: vlan10
-   extras:
    - ip-forward off
    - ip6-forward off
    id: 20
    name: vlan20
vni10:
    name: vni10
    vlan_id: 10
    vxlan_id: 10
vni20:
    name: vni20
    vlan_id: 20
    vxlan_id: 20
vnis:
-   name: vni10
    vlan_id: 10
    vxlan_id: 10
-   name: vni20
    vlan_id: 20
    vxlan_id: 20
vrf:
-   extras:
    - address 127.0.0.1/8
    - address ::1/128
    name: mgmt
    routes:
    - 0.0.0.0/0 192.168.200.1
vrf_mgmt:
    extras:
    - address 127.0.0.1/8
    - address ::1/128
    name: mgmt
    routes:
    - 0.0.0.0/0 192.168.200.1
vxlan_anycast_id: 12
vxlan_anycast_loopback: 10.0.1.12/32
vxlan_local_loopback: 10.10.10.1/32
...
</details>
###### evpn_mh
{'results': [{'cmd': ['ls', '/home/cumulus/cumulus_ansible_modules/inventories/evpn_centralized'], 'stdout': 'README.md\nconfig\ngroup_vars\nhost_vars\nhosts', 'stderr': '', 'rc': 0, 'start': '2021-02-16 09:58:18.804713', 'end': '2021-02-16 09:58:18.810627', 'delta': '0:00:00.005914', 'changed': True, 'invocation': {'module_args': {'_raw_params': 'ls /home/cumulus/cumulus_ansible_modules/inventories/evpn_centralized', 'warn': True, '_uses_shell': False, 'stdin_add_newline': True, 'strip_empty_ends': True, 'argv': None, 'chdir': None, 'executable': None, 'creates': None, 'removes': None, 'stdin': None}}, 'stdout_lines': ['README.md', 'config', 'group_vars', 'host_vars', 'hosts'], 'stderr_lines': [], 'failed': False, 'item': 'evpn_centralized', 'ansible_loop_var': 'item'}, {'cmd': ['ls', '/home/cumulus/cumulus_ansible_modules/inventories/evpn_l2only'], 'stdout': 'README.md\nconfig\ngroup_vars\nhost_vars\nhosts', 'stderr': '', 'rc': 0, 'start': '2021-02-16 09:58:19.130211', 'end': '2021-02-16 09:58:19.135650', 'delta': '0:00:00.005439', 'changed': True, 'invocation': {'module_args': {'_raw_params': 'ls /home/cumulus/cumulus_ansible_modules/inventories/evpn_l2only', 'warn': True, '_uses_shell': False, 'stdin_add_newline': True, 'strip_empty_ends': True, 'argv': None, 'chdir': None, 'executable': None, 'creates': None, 'removes': None, 'stdin': None}}, 'stdout_lines': ['README.md', 'config', 'group_vars', 'host_vars', 'hosts'], 'stderr_lines': [], 'failed': False, 'item': 'evpn_l2only', 'ansible_loop_var': 'item'}, {'cmd': ['ls', '/home/cumulus/cumulus_ansible_modules/inventories/evpn_mh'], 'stdout': 'config\ngroup_vars\nhost_vars\nhosts', 'stderr': '', 'rc': 0, 'start': '2021-02-16 09:58:19.456047', 'end': '2021-02-16 09:58:19.462062', 'delta': '0:00:00.006015', 'changed': True, 'invocation': {'module_args': {'_raw_params': 'ls /home/cumulus/cumulus_ansible_modules/inventories/evpn_mh', 'warn': True, '_uses_shell': False, 'stdin_add_newline': True, 'strip_empty_ends': True, 'argv': None, 'chdir': None, 'executable': None, 'creates': None, 'removes': None, 'stdin': None}}, 'stdout_lines': ['config', 'group_vars', 'host_vars', 'hosts'], 'stderr_lines': [], 'failed': False, 'item': 'evpn_mh', 'ansible_loop_var': 'item'}, {'cmd': ['ls', '/home/cumulus/cumulus_ansible_modules/inventories/evpn_symmetric'], 'stdout': 'README.md\nconfig\ngroup_vars\nhost_vars\nhosts', 'stderr': '', 'rc': 0, 'start': '2021-02-16 09:58:19.804893', 'end': '2021-02-16 09:58:19.810865', 'delta': '0:00:00.005972', 'changed': True, 'invocation': {'module_args': {'_raw_params': 'ls /home/cumulus/cumulus_ansible_modules/inventories/evpn_symmetric', 'warn': True, '_uses_shell': False, 'stdin_add_newline': True, 'strip_empty_ends': True, 'argv': None, 'chdir': None, 'executable': None, 'creates': None, 'removes': None, 'stdin': None}}, 'stdout_lines': ['README.md', 'config', 'group_vars', 'host_vars', 'hosts'], 'stderr_lines': [], 'failed': False, 'item': 'evpn_symmetric', 'ansible_loop_var': 'item'}], 'changed': True, 'msg': 'All items completed'}
<details>
  <summary markdown="span">leaf01 dns variables</summary>
---
ansible_check_mode: false
ansible_diff_mode: false
ansible_facts:
    discovered_interpreter_python: /usr/bin/python
ansible_forks: 50
ansible_inventory_sources:
- /home/cumulus/cumulus_ansible_modules/inventories/evpn_mh/hosts
ansible_playbook_python: /usr/bin/python3
ansible_run_tags:
- interfaces
- backup
- frr
- servers
ansible_skip_tags: []
ansible_ssh_pass: CumulusLinux!
ansible_user: cumulus
ansible_verbosity: 0
ansible_version:
    full: 2.9.13
    major: 2
    minor: 9
    revision: 13
    string: 2.9.13
backup:
    files:
    - /etc/network/interfaces
    - /etc/frr/frr.conf
    - /etc/frr/daemons
    path: ../inventories/evpn_mh/config/leaf01
bgp:
    address_family:
    -   name: ipv4_unicast
        redistribute:
        -   type: connected
    -   advertise_all_vni: true
        name: l2vpn_evpn
        neighbors:
        -   activate: true
            interface: underlay
    asn: '65101'
    neighbors:
    -   interface: swp51
        peergroup: underlay
        unnumbered: true
    -   interface: swp52
        peergroup: underlay
        unnumbered: true
    -   interface: swp53
        peergroup: underlay
        unnumbered: true
    -   interface: swp54
        peergroup: underlay
        unnumbered: true
    peergroups:
    -   name: underlay
        remote_as: external
    router_id: 10.10.10.1
    vrfs:
    -   address_family:
        -   name: ipv4_unicast
            redistribute:
            -   type: connected
        -   extras:
            - advertise ipv4 unicast
            name: l2vpn_evpn
        name: RED
        router_id: 10.10.10.1
    -   address_family:
        -   name: ipv4_unicast
            redistribute:
            -   type: connected
        -   extras:
            - advertise ipv4 unicast
            name: l2vpn_evpn
        name: BLUE
        router_id: 10.10.10.1
bgp_asn_prefix: 651
bonds:
-   bridge:
        access: 10
    es_df_pref: '50000'
    es_id: 1
    name: bond1
    options:
        extras:
        - bond-lacp-bypass-allow yes
        - mstpctl-bpduguard yes
        - mstpctl-portadminedge yes
        mtu: 9000
    ports:
    - swp1
-   bridge:
        access: 20
    es_df_pref: '50000'
    es_id: 2
    name: bond2
    options:
        extras:
        - bond-lacp-bypass-allow yes
        - mstpctl-bpduguard yes
        - mstpctl-portadminedge yes
        mtu: 9000
    ports:
    - swp2
-   bridge:
        access: 30
    es_df_pref: '50000'
    es_id: 3
    name: bond3
    options:
        extras:
        - bond-lacp-bypass-allow yes
        - mstpctl-bpduguard yes
        - mstpctl-portadminedge yes
        mtu: 9000
    ports:
    - swp3
bridge:
    vids:
    - 10
    - 20
    - 30
    - 4001
    - 4002
discovered_interpreter_python: /usr/bin/python
dns:
    domain: cumulusnetworks.com
    search_domain:
    - cumulusnetworks.com
    servers:
        ipv4:
        - 1.1.1.1
        - 8.8.8.8
        vrf: mgmt
es_df_pref: 50000
eth0:
    ips:
    - 192.168.200.11/24
eth0_id: 11
eth0_ip: 192.168.200.11/24
eth0_subnet: 192.168.200.0/24
evpn_mh:
    startup_delay: 10
    sysmac: 44:38:39:BE:EF:AA
evpn_mh_sysmac_prefix: 44:38:39:BE:EF
fabric_name: evpn_mh
group_names:
- leaf
- pod1
groups:
    all:
    - netq-ts
    - fw1
    - leaf01
    - leaf02
    - leaf03
    - leaf04
    - spine01
    - spine02
    - spine03
    - spine04
    - border01
    - border02
    - server01
    - server02
    - server03
    - server04
    - server05
    - server06
    border:
    - border01
    - border02
    fw:
    - fw1
    leaf:
    - leaf01
    - leaf02
    - leaf03
    - leaf04
    netq:
    - netq-ts
    pod1:
    - fw1
    - leaf01
    - leaf02
    - leaf03
    - leaf04
    - spine01
    - spine02
    - spine03
    - spine04
    - border01
    - border02
    - server01
    - server02
    - server03
    - server04
    - server05
    - server06
    server:
    - server01
    - server02
    - server03
    - server04
    - server05
    - server06
    spine:
    - spine01
    - spine02
    - spine03
    - spine04
    ungrouped: []
id: 1
interfaces:
-   name: swp51
    options:
        evpn_mh_uplink: true
        extras:
        - alias to spine
        pim: true
-   name: swp52
    options:
        evpn_mh_uplink: true
        extras:
        - alias to spine
        pim: true
-   name: swp53
    options:
        evpn_mh_uplink: true
        extras:
        - alias to spine
        pim: true
-   name: swp54
    options:
        evpn_mh_uplink: true
        extras:
        - alias to spine
        pim: true
inventory_dir: /home/cumulus/cumulus_ansible_modules/inventories/evpn_mh
inventory_file: /home/cumulus/cumulus_ansible_modules/inventories/evpn_mh/hosts
inventory_hostname: leaf01
inventory_hostname_short: leaf01
leaf_spine_interface:
    evpn_mh_uplink: true
    extras:
    - alias to spine
    pim: true
loopback:
    igmp: true
    ips:
    - 10.10.10.1/32
    pim:
        source: 10.10.10.1
    vxlan_local_tunnel_ip: 10.10.10.1
mac_prefix: 00:00:00:00:00
netq:
    agent_server: 192.168.200.250
    cli_access_key: long-key-0987654321
    cli_port: 443
    cli_premises: CITC
    cli_secret_key: long-key-1234567890
    cli_server: api.air.netq.cumulusnetworks.com
    version: latest
ntp:
    server_ips:
    - 0.cumulusnetworks.pool.ntp.org
    - 1.cumulusnetworks.pool.ntp.org
    - 2.cumulusnetworks.pool.ntp.org
    - 3.cumulusnetworks.pool.ntp.org
    timezone: America/Los_Angeles
pim:
    ecmp: true
    keep_alive: 3600
    rp_mcast_range: 224.0.0.0/4
    rpaddr: 10.10.100.100/32
    source: 10.10.10.1
playbook_dir: /home/cumulus/cumulus_ansible_modules/playbooks
snmp:
    addresses:
    - 192.168.200.11/24@mgmt
    - udp6:[::1]:161
    rocommunity: public
spine_leaf_interface:
    extras:
    - alias to leaf
    pim: true
ssh:
    banner: |-
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        Authorized Users Only!
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    motd: |-
        #########################################################
        Successfully logged in to: leaf01
        #########################################################
syslog:
    servers:
    - 192.168.200.1
sysmac: AA
tacacs:
    groups:
    -   name: admins
        priv_level: 15
    -   name: basics
        priv_level: 1
    secret: tacacskey
    server_ips:
    - 192.168.200.1
    users:
    -   group: basics
        name: basicuser
        password: password
    -   group: admins
        name: adminuser
        password: password
    vrf: mgmt
vlan10:
    address:
    - 10.1.10.2/24
    address_virtual:
    - 00:00:00:00:00:10 10.1.10.1/24
    id: 10
    name: vlan10
    vrf: RED
vlan101:
    address:
    - 10.1.101.2/24
    address_virtual:
    - 00:00:00:00:00:01 10.1.101.1/24
    id: 101
    name: vlan101
    vrf: RED
vlan101_subnet: 10.1.101.0/24
vlan102:
    address:
    - 10.1.102.2/24
    address_virtual:
    - 00:00:00:00:00:02 10.1.102.1/24
    id: 102
    name: vlan102
    vrf: BLUE
vlan102_subnet: 10.1.102.0/24
vlan10_subnet: 10.1.10.0/24
vlan20:
    address:
    - 10.1.20.2/24
    address_virtual:
    - 00:00:00:00:00:20 10.1.20.1/24
    id: 20
    name: vlan20
    vrf: RED
vlan20_subnet: 10.1.20.0/24
vlan30:
    address:
    - 10.1.30.2/24
    address_virtual:
    - 00:00:00:00:00:30 10.1.30.1/24
    id: 30
    name: vlan30
    vrf: BLUE
vlan30_subnet: 10.1.30.0/24
vlan4001:
    hwaddress: 44:38:39:BE:EF:AA
    id: 4001
    name: vlan4001
    vrf: RED
vlan4002:
    hwaddress: 44:38:39:BE:EF:AA
    id: 4002
    name: vlan4002
    vrf: BLUE
vlans:
-   address:
    - 10.1.10.2/24
    address_virtual:
    - 00:00:00:00:00:10 10.1.10.1/24
    id: 10
    name: vlan10
    vrf: RED
-   address:
    - 10.1.20.2/24
    address_virtual:
    - 00:00:00:00:00:20 10.1.20.1/24
    id: 20
    name: vlan20
    vrf: RED
-   address:
    - 10.1.30.2/24
    address_virtual:
    - 00:00:00:00:00:30 10.1.30.1/24
    id: 30
    name: vlan30
    vrf: BLUE
-   hwaddress: 44:38:39:BE:EF:AA
    id: 4001
    name: vlan4001
    vrf: RED
-   hwaddress: 44:38:39:BE:EF:AA
    id: 4002
    name: vlan4002
    vrf: BLUE
vni10:
    extras:
    - vxlan-mcastgrp 224.0.0.10
    name: vni10
    vlan_id: 10
    vxlan_id: 10
vni101:
    extras:
    - vxlan-mcastgrp 224.0.0.101
    name: vni101
    vlan_id: 101
    vxlan_id: 101
vni102:
    extras:
    - vxlan-mcastgrp 224.0.0.102
    name: vni102
    vlan_id: 102
    vxlan_id: 102
vni20:
    extras:
    - vxlan-mcastgrp 224.0.0.20
    name: vni20
    vlan_id: 20
    vxlan_id: 20
vni30:
    extras:
    - vxlan-mcastgrp 224.0.0.30
    name: vni30
    vlan_id: 30
    vxlan_id: 30
vniBLUE:
    name: vniBLUE
    vlan_id: 4002
    vxlan_id: 4002
vniRED:
    name: vniRED
    vlan_id: 4001
    vxlan_id: 4001
vnis:
-   extras:
    - vxlan-mcastgrp 224.0.0.10
    name: vni10
    vlan_id: 10
    vxlan_id: 10
-   extras:
    - vxlan-mcastgrp 224.0.0.20
    name: vni20
    vlan_id: 20
    vxlan_id: 20
-   extras:
    - vxlan-mcastgrp 224.0.0.30
    name: vni30
    vlan_id: 30
    vxlan_id: 30
-   name: vniRED
    vlan_id: 4001
    vxlan_id: 4001
-   name: vniBLUE
    vlan_id: 4002
    vxlan_id: 4002
vrf:
-   extras:
    - address 127.0.0.1/8
    - address ::1/128
    name: mgmt
    routes:
    - 0.0.0.0/0 192.168.200.1
-   name: RED
    vxlan_id: 4001
-   name: BLUE
    vxlan_id: 4002
vrf_BLUE:
    name: BLUE
    vxlan_id: 4002
vrf_BLUE_border:
    name: BLUE
    routes:
    - 10.1.10.0/24 10.1.102.4
    - 10.1.20.0/24 10.1.102.4
    vxlan_id: 4002
vrf_RED:
    name: RED
    vxlan_id: 4001
vrf_RED_border:
    name: RED
    routes:
    - 10.1.30.0/24 10.1.101.4
    vxlan_id: 4001
vrf_default_FW:
    name: default
    routes:
    - 10.1.10.0/24 10.1.101.1
    - 10.1.20.0/24 10.1.101.1
    - 10.1.30.0/24 10.1.102.1
vrf_mgmt:
    extras:
    - address 127.0.0.1/8
    - address ::1/128
    name: mgmt
    routes:
    - 0.0.0.0/0 192.168.200.1
vxlan_local_loopback: 10.10.10.1/32
...
</details>
###### evpn_symmetric
{'results': [{'cmd': ['ls', '/home/cumulus/cumulus_ansible_modules/inventories/evpn_centralized'], 'stdout': 'README.md\nconfig\ngroup_vars\nhost_vars\nhosts', 'stderr': '', 'rc': 0, 'start': '2021-02-16 09:58:18.804713', 'end': '2021-02-16 09:58:18.810627', 'delta': '0:00:00.005914', 'changed': True, 'invocation': {'module_args': {'_raw_params': 'ls /home/cumulus/cumulus_ansible_modules/inventories/evpn_centralized', 'warn': True, '_uses_shell': False, 'stdin_add_newline': True, 'strip_empty_ends': True, 'argv': None, 'chdir': None, 'executable': None, 'creates': None, 'removes': None, 'stdin': None}}, 'stdout_lines': ['README.md', 'config', 'group_vars', 'host_vars', 'hosts'], 'stderr_lines': [], 'failed': False, 'item': 'evpn_centralized', 'ansible_loop_var': 'item'}, {'cmd': ['ls', '/home/cumulus/cumulus_ansible_modules/inventories/evpn_l2only'], 'stdout': 'README.md\nconfig\ngroup_vars\nhost_vars\nhosts', 'stderr': '', 'rc': 0, 'start': '2021-02-16 09:58:19.130211', 'end': '2021-02-16 09:58:19.135650', 'delta': '0:00:00.005439', 'changed': True, 'invocation': {'module_args': {'_raw_params': 'ls /home/cumulus/cumulus_ansible_modules/inventories/evpn_l2only', 'warn': True, '_uses_shell': False, 'stdin_add_newline': True, 'strip_empty_ends': True, 'argv': None, 'chdir': None, 'executable': None, 'creates': None, 'removes': None, 'stdin': None}}, 'stdout_lines': ['README.md', 'config', 'group_vars', 'host_vars', 'hosts'], 'stderr_lines': [], 'failed': False, 'item': 'evpn_l2only', 'ansible_loop_var': 'item'}, {'cmd': ['ls', '/home/cumulus/cumulus_ansible_modules/inventories/evpn_mh'], 'stdout': 'config\ngroup_vars\nhost_vars\nhosts', 'stderr': '', 'rc': 0, 'start': '2021-02-16 09:58:19.456047', 'end': '2021-02-16 09:58:19.462062', 'delta': '0:00:00.006015', 'changed': True, 'invocation': {'module_args': {'_raw_params': 'ls /home/cumulus/cumulus_ansible_modules/inventories/evpn_mh', 'warn': True, '_uses_shell': False, 'stdin_add_newline': True, 'strip_empty_ends': True, 'argv': None, 'chdir': None, 'executable': None, 'creates': None, 'removes': None, 'stdin': None}}, 'stdout_lines': ['config', 'group_vars', 'host_vars', 'hosts'], 'stderr_lines': [], 'failed': False, 'item': 'evpn_mh', 'ansible_loop_var': 'item'}, {'cmd': ['ls', '/home/cumulus/cumulus_ansible_modules/inventories/evpn_symmetric'], 'stdout': 'README.md\nconfig\ngroup_vars\nhost_vars\nhosts', 'stderr': '', 'rc': 0, 'start': '2021-02-16 09:58:19.804893', 'end': '2021-02-16 09:58:19.810865', 'delta': '0:00:00.005972', 'changed': True, 'invocation': {'module_args': {'_raw_params': 'ls /home/cumulus/cumulus_ansible_modules/inventories/evpn_symmetric', 'warn': True, '_uses_shell': False, 'stdin_add_newline': True, 'strip_empty_ends': True, 'argv': None, 'chdir': None, 'executable': None, 'creates': None, 'removes': None, 'stdin': None}}, 'stdout_lines': ['README.md', 'config', 'group_vars', 'host_vars', 'hosts'], 'stderr_lines': [], 'failed': False, 'item': 'evpn_symmetric', 'ansible_loop_var': 'item'}], 'changed': True, 'msg': 'All items completed'}
<details>
  <summary markdown="span">leaf01 dns variables</summary>
---
ansible_check_mode: false
ansible_diff_mode: false
ansible_facts:
    discovered_interpreter_python: /usr/bin/python
ansible_forks: 50
ansible_inventory_sources:
- /home/cumulus/cumulus_ansible_modules/inventories/evpn_symmetric/hosts
ansible_playbook_python: /usr/bin/python3
ansible_run_tags:
- backup
- frr
- servers
- interfaces
ansible_skip_tags: []
ansible_ssh_pass: CumulusLinux!
ansible_user: cumulus
ansible_verbosity: 0
ansible_version:
    full: 2.9.13
    major: 2
    minor: 9
    revision: 13
    string: 2.9.13
backup:
    files:
    - /etc/network/interfaces
    - /etc/frr/frr.conf
    - /etc/frr/daemons
    path: ../inventories/evpn_symmetric/config/leaf01
bgp:
    address_family:
    -   name: ipv4_unicast
        redistribute:
        -   type: connected
    -   advertise_all_vni: true
        name: l2vpn_evpn
        neighbors:
        -   activate: true
            interface: underlay
    asn: '65101'
    neighbors:
    -   interface: peerlink.4094
        peergroup: underlay
        unnumbered: true
    -   interface: swp51
        peergroup: underlay
        unnumbered: true
    -   interface: swp52
        peergroup: underlay
        unnumbered: true
    -   interface: swp53
        peergroup: underlay
        unnumbered: true
    -   interface: swp54
        peergroup: underlay
        unnumbered: true
    peergroups:
    -   name: underlay
        remote_as: external
    router_id: 10.10.10.1
    vrfs:
    -   address_family:
        -   name: ipv4_unicast
            redistribute:
            -   type: connected
        -   extras:
            - advertise ipv4 unicast
            name: l2vpn_evpn
        name: RED
        router_id: 10.10.10.1
    -   address_family:
        -   name: ipv4_unicast
            redistribute:
            -   type: connected
        -   extras:
            - advertise ipv4 unicast
            name: l2vpn_evpn
        name: BLUE
        router_id: 10.10.10.1
bgp_asn_prefix: 651
bonds:
-   bridge:
        access: 10
    clag_id: 1
    name: bond1
    options:
        extras:
        - bond-lacp-bypass-allow yes
        - mstpctl-bpduguard yes
        - mstpctl-portadminedge yes
        mtu: 9000
    ports:
    - swp1
-   bridge:
        access: 20
    clag_id: 2
    name: bond2
    options:
        extras:
        - bond-lacp-bypass-allow yes
        - mstpctl-bpduguard yes
        - mstpctl-portadminedge yes
        mtu: 9000
    ports:
    - swp2
-   bridge:
        access: 30
    clag_id: 3
    name: bond3
    options:
        extras:
        - bond-lacp-bypass-allow yes
        - mstpctl-bpduguard yes
        - mstpctl-portadminedge yes
        mtu: 9000
    ports:
    - swp3
bridge:
    vids:
    - 10
    - 20
    - 30
    - 4001
    - 4002
discovered_interpreter_python: /usr/bin/python
dns:
    domain: cumulusnetworks.com
    search_domain:
    - cumulusnetworks.com
    servers:
        ipv4:
        - 1.1.1.1
        - 8.8.8.8
        vrf: mgmt
eth0:
    ips:
    - 192.168.200.11/24
eth0_id: 11
eth0_ip: 192.168.200.11/24
eth0_subnet: 192.168.200.0/24
fabric_name: evpn_symmetric
group_names:
- leaf
- pod1
groups:
    all:
    - netq-ts
    - fw1
    - leaf01
    - leaf02
    - leaf03
    - leaf04
    - spine01
    - spine02
    - spine03
    - spine04
    - border01
    - border02
    - server01
    - server02
    - server03
    - server04
    - server05
    - server06
    border:
    - border01
    - border02
    fw:
    - fw1
    leaf:
    - leaf01
    - leaf02
    - leaf03
    - leaf04
    netq:
    - netq-ts
    pod1:
    - fw1
    - leaf01
    - leaf02
    - leaf03
    - leaf04
    - spine01
    - spine02
    - spine03
    - spine04
    - border01
    - border02
    - server01
    - server02
    - server03
    - server04
    - server05
    - server06
    server:
    - server01
    - server02
    - server03
    - server04
    - server05
    - server06
    spine:
    - spine01
    - spine02
    - spine03
    - spine04
    ungrouped: []
id: 1
interfaces:
-   name: swp51
    options:
        extras:
        - alias to spine
-   name: swp52
    options:
        extras:
        - alias to spine
-   name: swp53
    options:
        extras:
        - alias to spine
-   name: swp54
    options:
        extras:
        - alias to spine
inventory_dir: /home/cumulus/cumulus_ansible_modules/inventories/evpn_symmetric
inventory_file: /home/cumulus/cumulus_ansible_modules/inventories/evpn_symmetric/hosts
inventory_hostname: leaf01
inventory_hostname_short: leaf01
leaf_spine_interface:
    extras:
    - alias to spine
loopback:
    clag_vxlan_anycast_ip: 10.0.1.12
    ips:
    - 10.10.10.1/32
    vxlan_local_tunnel_ip: 10.10.10.1
mac_prefix: 00:00:00:00:00
mlag:
    backup: 10.10.10.2
    init_delay: 10
    peerlinks:
    - swp49
    - swp50
    priority: primary
    sysmac: 44:38:39:BE:EF:AA
mlagBondProfileA:
    bridge_access: 10
    extras:
    - bond-lacp-bypass-allow yes
    - mstpctl-bpduguard yes
    - mstpctl-portadminedge yes
mlagBondProfileB:
    bridge_access: 20
    extras:
    - bond-lacp-bypass-allow yes
    - mstpctl-bpduguard yes
    - mstpctl-portadminedge yes
mlagBondProfileC:
    bridge_access: 30
    extras:
    - bond-lacp-bypass-allow yes
    - mstpctl-bpduguard yes
    - mstpctl-portadminedge yes
mlagBondProfileFW:
    bridge_vids:
    - 101
    - 102
mlag_neighbor: leaf02
mlag_priority: primary
mlag_sysmac_prefix: 44:38:39:BE:EF
netq:
    agent_server: 192.168.200.250
    cli_access_key: long-key-0987654321
    cli_port: 443
    cli_premises: CITC
    cli_secret_key: long-key-1234567890
    cli_server: api.air.netq.cumulusnetworks.com
    version: latest
ntp:
    server_ips:
    - 0.cumulusnetworks.pool.ntp.org
    - 1.cumulusnetworks.pool.ntp.org
    - 2.cumulusnetworks.pool.ntp.org
    - 3.cumulusnetworks.pool.ntp.org
    timezone: America/Los_Angeles
playbook_dir: /home/cumulus/cumulus_ansible_modules/playbooks
snmp:
    addresses:
    - 192.168.200.11/24@mgmt
    - udp6:[::1]:161
    rocommunity: public
spine_leaf_interface:
    extras:
    - alias to leaf
ssh:
    banner: |-
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        Authorized Users Only!
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    motd: |-
        #########################################################
        Successfully logged in to: leaf01
        #########################################################
syslog:
    servers:
    - 192.168.200.1
sysmac: AA
tacacs:
    groups:
    -   name: admins
        priv_level: 15
    -   name: basics
        priv_level: 1
    secret: tacacskey
    server_ips:
    - 192.168.200.1
    users:
    -   group: basics
        name: basicuser
        password: password
    -   group: admins
        name: adminuser
        password: password
    vrf: mgmt
vlan10:
    address:
    - 10.1.10.2/24
    address_virtual:
    - 00:00:00:00:00:10 10.1.10.1/24
    id: 10
    name: vlan10
    vrf: RED
vlan101:
    address:
    - 10.1.101.2/24
    address_virtual:
    - 00:00:00:00:00:01 10.1.101.1/24
    id: 101
    name: vlan101
    vrf: RED
vlan101_subnet: 10.1.101.0/24
vlan102:
    address:
    - 10.1.102.2/24
    address_virtual:
    - 00:00:00:00:00:02 10.1.102.1/24
    id: 102
    name: vlan102
    vrf: BLUE
vlan102_subnet: 10.1.102.0/24
vlan10_subnet: 10.1.10.0/24
vlan20:
    address:
    - 10.1.20.2/24
    address_virtual:
    - 00:00:00:00:00:20 10.1.20.1/24
    id: 20
    name: vlan20
    vrf: RED
vlan20_subnet: 10.1.20.0/24
vlan30:
    address:
    - 10.1.30.2/24
    address_virtual:
    - 00:00:00:00:00:30 10.1.30.1/24
    id: 30
    name: vlan30
    vrf: BLUE
vlan30_subnet: 10.1.30.0/24
vlan4001:
    hwaddress: 44:38:39:BE:EF:AA
    id: 4001
    name: vlan4001
    vrf: RED
vlan4002:
    hwaddress: 44:38:39:BE:EF:AA
    id: 4002
    name: vlan4002
    vrf: BLUE
vlans:
-   address:
    - 10.1.10.2/24
    address_virtual:
    - 00:00:00:00:00:10 10.1.10.1/24
    id: 10
    name: vlan10
    vrf: RED
-   address:
    - 10.1.20.2/24
    address_virtual:
    - 00:00:00:00:00:20 10.1.20.1/24
    id: 20
    name: vlan20
    vrf: RED
-   address:
    - 10.1.30.2/24
    address_virtual:
    - 00:00:00:00:00:30 10.1.30.1/24
    id: 30
    name: vlan30
    vrf: BLUE
-   hwaddress: 44:38:39:BE:EF:AA
    id: 4001
    name: vlan4001
    vrf: RED
-   hwaddress: 44:38:39:BE:EF:AA
    id: 4002
    name: vlan4002
    vrf: BLUE
vni10:
    name: vni10
    vlan_id: 10
    vxlan_id: 10
vni20:
    name: vni20
    vlan_id: 20
    vxlan_id: 20
vni30:
    name: vni30
    vlan_id: 30
    vxlan_id: 30
vniBLUE:
    name: vniBLUE
    vlan_id: 4002
    vxlan_id: 4002
vniRED:
    name: vniRED
    vlan_id: 4001
    vxlan_id: 4001
vnis:
-   name: vni10
    vlan_id: 10
    vxlan_id: 10
-   name: vni20
    vlan_id: 20
    vxlan_id: 20
-   name: vni30
    vlan_id: 30
    vxlan_id: 30
-   name: vniRED
    vlan_id: 4001
    vxlan_id: 4001
-   name: vniBLUE
    vlan_id: 4002
    vxlan_id: 4002
vrf:
-   extras:
    - address 127.0.0.1/8
    - address ::1/128
    name: mgmt
    routes:
    - 0.0.0.0/0 192.168.200.1
-   name: RED
    vxlan_id: 4001
-   name: BLUE
    vxlan_id: 4002
vrf_BLUE:
    name: BLUE
    vxlan_id: 4002
vrf_BLUE_border:
    name: BLUE
    routes:
    - 10.1.10.0/24 10.1.102.4
    - 10.1.20.0/24 10.1.102.4
    vxlan_id: 4002
vrf_RED:
    name: RED
    vxlan_id: 4001
vrf_RED_border:
    name: RED
    routes:
    - 10.1.30.0/24 10.1.101.4
    vxlan_id: 4001
vrf_default_FW:
    name: default
    routes:
    - 10.1.10.0/24 10.1.101.1
    - 10.1.20.0/24 10.1.101.1
    - 10.1.30.0/24 10.1.102.1
vrf_mgmt:
    extras:
    - address 127.0.0.1/8
    - address ::1/128
    name: mgmt
    routes:
    - 0.0.0.0/0 192.168.200.1
vxlan_anycast_id: 12
vxlan_anycast_loopback: 10.0.1.12/32
vxlan_local_loopback: 10.10.10.1/32
...
</details>

